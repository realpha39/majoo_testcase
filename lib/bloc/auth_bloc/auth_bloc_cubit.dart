import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/database_service.dart';

import '../../common/widget/text_form_field.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

  final emailController = TextController();
  final passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void createDatabase() async {
    emit(AuthBlocLoginState());
    await DatabaseService.createDb();
    emit(AuthBlocSuccesState());
  }

  void loginUser() async {
    emit(AuthBlocLoadingState());
    final result = await DatabaseService.getData(
      email: emailController.value!,
      password: passwordController.value!,
    );
    if (result) {
      emit(AuthBlocLoggedInState());
    } else {
      emit(
        const AuthBlocErrorState('Login gagal, periksa kembali inputan anda'),
      );
    }
  }
}
