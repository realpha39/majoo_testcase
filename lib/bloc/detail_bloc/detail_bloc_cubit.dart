import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/detail_movie/detail_movie.dart';
import 'package:majootestcase/services/api_service.dart';

part 'detail_bloc_state.dart';

class DetailBlocCubit extends Cubit<DetailBlocState> {
  DetailBlocCubit() : super(DetailBlocInitial());

  final ApiServices _apiServices = ApiServices();

  void getDetailMovie(int id) async {
    emit(DetailBlocLoading());
    final response = await _apiServices.getMovieDetail(id);
    if (response == null) {
      emit(const DetailBlocError("Error"));
    } else {
      DetailMovie detailMovie = response;
      emit(DetailBlocSuccess(detailMovie));
    }
  }
}
