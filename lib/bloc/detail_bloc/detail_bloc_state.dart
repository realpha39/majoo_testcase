part of 'detail_bloc_cubit.dart';

abstract class DetailBlocState extends Equatable {
  const DetailBlocState();

  @override
  List<Object> get props => [];
}

class DetailBlocInitial extends DetailBlocState {}

class DetailBlocLoading extends DetailBlocState {}

class DetailBlocError extends DetailBlocState {
  final String error;

  const DetailBlocError(this.error);

  @override
  List<Object> get props => [error];
}

class DetailBlocSuccess extends DetailBlocState {
  final DetailMovie detailMovie;

  const DetailBlocSuccess(this.detailMovie);

  @override
  List<Object> get props => [detailMovie];
}
