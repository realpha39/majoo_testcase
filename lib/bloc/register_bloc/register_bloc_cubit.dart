import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/user.dart';

import '../../common/widget/text_form_field.dart';
import '../../services/database_service.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitial());

  final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

  final emailController = TextController();
  final usernameController = TextController();
  final passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void registerUser() async {
    emit(RegisterBlocLoading());
    User user = User(
      email: emailController.value!,
      password: passwordController.value!,
    );
    await DatabaseService.insertData(user: user);
    emit(RegisterBlocSuccess());
  }
}
