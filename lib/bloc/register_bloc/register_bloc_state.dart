part of 'register_bloc_cubit.dart';

abstract class RegisterBlocState extends Equatable {
  const RegisterBlocState();

  @override
  List<Object> get props => [];
}

class RegisterBlocInitial extends RegisterBlocState {}

class RegisterBlocLoading extends RegisterBlocState {}

class RegisterBlocError extends RegisterBlocState {
  final String error;

  const RegisterBlocError(this.error);

  @override
  List<Object> get props => [error];
}

class RegisterBlocSuccess extends RegisterBlocState {}
