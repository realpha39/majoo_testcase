import 'dart:convert';

import 'belongs_to_collection.dart';
import 'genre.dart';
import 'production_company.dart';
import 'production_country.dart';
import 'spoken_language.dart';

class DetailMovie {
  bool? adult;
  String? backdropPath;
  BelongsToCollection? belongsToCollection;
  int? budget;
  List<Genre>? genres;
  String? homepage;
  int? id;
  String? imdbId;
  String? originalLanguage;
  String? originalTitle;
  String? overview;
  double? popularity;
  String? posterPath;
  List<ProductionCompany>? productionCompanies;
  List<ProductionCountry>? productionCountries;
  String? releaseDate;
  int? revenue;
  int? runtime;
  List<SpokenLanguage>? spokenLanguages;
  String? status;
  String? tagline;
  String? title;
  bool? video;
  double? voteAverage;
  int? voteCount;

  DetailMovie({
    this.adult,
    this.backdropPath,
    this.belongsToCollection,
    this.budget,
    this.genres,
    this.homepage,
    this.id,
    this.imdbId,
    this.originalLanguage,
    this.originalTitle,
    this.overview,
    this.popularity,
    this.posterPath,
    this.productionCompanies,
    this.productionCountries,
    this.releaseDate,
    this.revenue,
    this.runtime,
    this.spokenLanguages,
    this.status,
    this.tagline,
    this.title,
    this.video,
    this.voteAverage,
    this.voteCount,
  });

  factory DetailMovie.fromMap(Map<String, dynamic> data) => DetailMovie(
        adult: data['adult'] ?? false,
        backdropPath: data['backdrop_path'] ?? '',
        belongsToCollection: data['belongs_to_collection'] == null
            ? BelongsToCollection()
            : BelongsToCollection.fromMap(
                data['belongs_to_collection'] as Map<String, dynamic>),
        budget: data['budget'] ?? 0,
        genres: (data['genres'] ?? [])
            .map((e) => Genre.fromMap(e as Map<String, dynamic>))
            .toList()
            .cast<Genre>(),
        homepage: data['homepage'] ?? '',
        id: data['id'] ?? 0,
        imdbId: data['imdb_id'] ?? '',
        originalLanguage: data['original_language'] ?? '',
        originalTitle: data['original_title'] ?? '',
        overview: data['overview'] ?? '',
        popularity: data['popularity'] ?? 0.0,
        posterPath: data['poster_path'] ?? '',
        productionCompanies: (data['production_companies'] ?? [])
            .map((e) => ProductionCompany.fromMap(e as Map<String, dynamic>))
            .toList()
            .cast<ProductionCompany>(),
        productionCountries: (data['production_countries'] ?? [])
            .map((e) => ProductionCountry.fromMap(e as Map<String, dynamic>))
            .toList()
            .cast<ProductionCountry>(),
        releaseDate: data['release_date'] ?? '',
        revenue: data['revenue'] ?? 0,
        runtime: data['runtime'] ?? 0,
        spokenLanguages: (data['spoken_languages'] ?? [])
            .map((e) => SpokenLanguage.fromMap(e as Map<String, dynamic>))
            .toList()
            .cast<SpokenLanguage>(),
        status: data['status'] ?? '',
        tagline: data['tagline'] ?? '',
        title: data['title'] ?? '',
        video: data['video'] ?? false,
        voteAverage: data['vote_average'] ?? 0.0,
        voteCount: data['vote_count'] ?? 0,
      );

  Map<String, dynamic> toMap() => {
        'adult': adult,
        'backdrop_path': backdropPath,
        'belongs_to_collection': belongsToCollection?.toMap(),
        'budget': budget,
        'genres': genres?.map((e) => e.toMap()).toList(),
        'homepage': homepage,
        'id': id,
        'imdb_id': imdbId,
        'original_language': originalLanguage,
        'original_title': originalTitle,
        'overview': overview,
        'popularity': popularity,
        'poster_path': posterPath,
        'production_companies':
            productionCompanies?.map((e) => e.toMap()).toList(),
        'production_countries':
            productionCountries?.map((e) => e.toMap()).toList(),
        'release_date': releaseDate,
        'revenue': revenue,
        'runtime': runtime,
        'spoken_languages': spokenLanguages?.map((e) => e.toMap()).toList(),
        'status': status,
        'tagline': tagline,
        'title': title,
        'video': video,
        'vote_average': voteAverage,
        'vote_count': voteCount,
      };

  /// `dart:convert`
  ///
  /// Parses the string and returns the resulting Json object as [DetailMovie].
  factory DetailMovie.fromJson(String data) {
    return DetailMovie.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  /// `dart:convert`
  ///
  /// Converts [DetailMovie] to a JSON string.
  String toJson() => json.encode(toMap());
}
