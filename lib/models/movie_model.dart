class MovieModel {
  int? page;
  List<Results>? results;
  int? totalPages;
  int? totalResults;

  MovieModel({this.page, results, totalPages, totalResults});

  MovieModel.fromJson(Map<String, dynamic> json) {
    page = json['page'] ?? '';
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results!.add(Results.fromJson(v));
      });
    }
    totalPages = json['total_pages'] ?? '';
    totalResults = json['total_results'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['page'] = page;
    if (results != null) {
      data['results'] = results!.map((v) => v.toJson()).toList();
    }
    data['total_pages'] = totalPages;
    data['total_results'] = totalResults;
    return data;
  }
}

class Results {
  double? voteAverage;
  String? overview;
  String? releaseDate;
  bool? adult;
  String? backdropPath;
  int? voteCount;
  List<int>? genreIds;
  int? id;
  String? originalLanguage;
  String? originalTitle;
  String? posterPath;
  String? title;
  bool? video;
  double? popularity;
  String? mediaType;
  String? name;
  String? originalName;
  List<String>? originCountry;
  String? firstAirDate;

  Results(
      {this.voteAverage,
      this.overview,
      this.releaseDate,
      this.adult,
      this.backdropPath,
      this.voteCount,
      this.genreIds,
      this.id,
      this.originalLanguage,
      this.originalTitle,
      this.posterPath,
      this.title,
      this.video,
      this.popularity,
      this.mediaType,
      this.name,
      this.originalName,
      this.originCountry,
      this.firstAirDate});

  Results.fromJson(Map<String, dynamic> json) {
    voteAverage = json['vote_average'] ?? '';
    overview = json['overview'] ?? '';
    releaseDate = json['release_date'] ?? '';
    adult = json['adult'] ?? false;
    backdropPath = json['backdrop_path'] ?? '';
    voteCount = json['vote_count'] ?? '';
    genreIds = json['genre_ids'] == null ? [0] : json['genre_ids'].cast<int>();
    id = json['id'] ?? '';
    originalLanguage = json['original_language'] ?? '';
    originalTitle = json['original_title'] ?? '';
    posterPath = json['poster_path'] ?? '';
    title = json['title'] ?? '';
    video = json['video'] ?? false;
    popularity = json['popularity'] ?? '';
    mediaType = json['media_type'] ?? '';
    name = json['name'] ?? '';
    originalName = json['original_name'] ?? '';
    originCountry = json['origin_country'] == null
        ? ['']
        : json['origin_country'].cast<String>();
    firstAirDate = json['first_air_date'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['vote_average'] = voteAverage;
    data['overview'] = overview;
    data['release_date'] = releaseDate;
    data['adult'] = adult;
    data['backdrop_path'] = backdropPath;
    data['vote_count'] = voteCount;
    data['genre_ids'] = genreIds;
    data['id'] = id;
    data['original_language'] = originalLanguage;
    data['original_title'] = originalTitle;
    data['poster_path'] = posterPath;
    data['title'] = title;
    data['video'] = video;
    data['popularity'] = popularity;
    data['media_type'] = mediaType;
    data['name'] = name;
    data['original_name'] = originalName;
    data['origin_country'] = originCountry;
    data['first_air_date'] = firstAirDate;
    return data;
  }
}
