import 'package:dio/dio.dart';
import 'package:majootestcase/models/detail_movie/detail_movie.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dio_config;
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/error_helper.dart';

class ApiServices {
  Future<MovieModel?> getMovieList() async {
    try {
      var dio = await dio_config.dio();
      final response = await dio.get(
        'trending/all/day',
        queryParameters: {
          'api_key': Api.apiKey,
        },
      );

      if (response.statusCode == 200) {
        final data = MovieModel.fromJson(response.data);
        return data;
      } else {
        return null;
      }
    } on DioError catch (e) {
      ErrorHelper.extractApiError(e);
      return null;
    }
  }

  Future<DetailMovie?> getMovieDetail(int id) async {
    try {
      final dio = await dio_config.dio();
      final response = await dio.get(
        'movie/$id',
        queryParameters: {'api_key': Api.apiKey},
      );

      if (response.statusCode != 200) {
        return null;
      }

      final data = DetailMovie.fromMap(response.data);
      return data;
    } on DioError catch (e) {
      ErrorHelper.extractApiError(e);
      return null;
    }
  }
}
