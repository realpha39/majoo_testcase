import 'dart:io';

import 'package:majootestcase/models/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  static late Database database;

  static Future<void> createDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'user.db';
    database = await openDatabase(path);
    await createTable();
  }

  static Future<void> createTable() async {
    final result = await database
        .query('sqlite_master', where: 'name = ?', whereArgs: ['USER']);
    if (result.isEmpty) {
      await database.execute(
          'CREATE TABLE USER(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT, email TEXT, password TEXT)');
    }
  }

  static Future<void> insertData({required User user}) async {
    await database.rawInsert(
        'INSERT INTO USER(email, password) VALUES("${user.email}", "${user.password}")');
  }

  static Future<bool> getData(
      {required String email, required String password}) async {
    List<Map<String, dynamic>> list = await database.rawQuery(
        'SELECT * FROM USER WHERE email = "$email" AND password = "$password"');

    if (list.isEmpty) {
      return false;
    }
    return true;
  }

  static Future<void> closeDb() {
    return database.close();
  }
}
