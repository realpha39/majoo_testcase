import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_screen_loaded.dart';
import 'package:majootestcase/ui/extra/loading.dart';

import '../../bloc/detail_bloc/detail_bloc_cubit.dart';
import '../extra/error_screen.dart';

class DetailMovieScreen extends StatelessWidget {
  final int id;

  const DetailMovieScreen({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DetailBlocCubit()..getDetailMovie(id),
      child: Material(
        child: _DetailMovieScreen(id: id),
      ),
    );
  }
}

class _DetailMovieScreen extends StatelessWidget {
  final int id;
  const _DetailMovieScreen({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailBlocCubit, DetailBlocState>(
      builder: (context, state) {
        if (state is DetailBlocLoading) {
          return const LoadingIndicator();
        } else if (state is DetailBlocError) {
          return ErrorScreen(
            message: state.error,
            retry: () {},
            retryButton: OutlinedButton(
              child: const Text('Refresh'),
              onPressed: () =>
                  context.read<DetailBlocCubit>().getDetailMovie(id),
            ),
          );
        } else if (state is DetailBlocSuccess) {
          return DetailMovieScreenLoaded(data: state.detailMovie);
        } else {
          return const SizedBox();
        }
      },
    );
  }
}
