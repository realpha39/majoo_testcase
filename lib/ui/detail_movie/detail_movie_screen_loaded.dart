import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

import '../../models/detail_movie/detail_movie.dart';
import 'widgets/synopsis_card.dart';
import 'widgets/upper_detail_movie_card.dart';

class DetailMovieScreenLoaded extends StatelessWidget {
  final DetailMovie data;
  const DetailMovieScreenLoaded({Key? key, required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorStyle.backgroundColor,
      body: Column(
        children: [
          UpperDetailMovieCard(data: data),
          const SizedBox(height: 16),
          SynopsisCard(data: data)
        ],
      ),
    );
  }
}
