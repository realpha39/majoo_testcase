import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/models/detail_movie/detail_movie.dart';
import 'package:majootestcase/utils/constant.dart';

class SynopsisCard extends StatelessWidget {
  final DetailMovie data;
  const SynopsisCard({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32),
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(50),
            topLeft: Radius.circular(50),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: ColorStyle.bgGreyColor,
              blurRadius: 10,
              spreadRadius: 7,
              offset: Offset(0, 0),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
            top: 16,
            bottom: 24,
            right: 16,
            left: 42,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Sinopsis',
                style: TextStyle(fontSize: 12.sp),
                textAlign: TextAlign.justify,
              ),
              const SizedBox(height: 16),
              Text(
                data.overview!,
                style: TextStyle(fontSize: 12.sp),
                textAlign: TextAlign.justify,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
