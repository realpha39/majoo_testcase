import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/models/detail_movie/detail_movie.dart';
import 'package:majootestcase/utils/constant.dart';

class UpperDetailMovieCard extends StatelessWidget {
  final DetailMovie data;
  const UpperDetailMovieCard({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(70),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: ColorStyle.bgGreyColor,
              blurRadius: 10,
              spreadRadius: 7,
              offset: Offset(0, 0),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Text(
                data.title!,
                style: TextStyle(fontSize: 14.sp),
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                          color: ColorStyle.bgGreyColor,
                          blurRadius: 7,
                          spreadRadius: 3,
                          offset: Offset(0, 0),
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.network(
                        "https://image.tmdb.org/t/p/w500/" + data.posterPath!,
                        fit: BoxFit.cover,
                        width: 100.w,
                      ),
                    ),
                  ),
                  const SizedBox(width: 12),
                  Row(
                    children: [
                      Column(
                        children: [
                          const Text('Rating'),
                          const SizedBox(height: 4),
                          Text(
                            data.voteAverage!.toString(),
                            style: TextStyle(fontSize: 32.sp),
                          ),
                        ],
                      ),
                      const SizedBox(width: 12),
                      Container(
                        height: 80.h,
                        color: Colors.grey,
                        width: 1,
                      ),
                      const SizedBox(width: 12),
                      Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Tanggal Rilis',
                                style: TextStyle(fontSize: 10.sp),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                'Voting Total',
                                style: TextStyle(fontSize: 10.sp),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                'Popularitas',
                                style: TextStyle(fontSize: 10.sp),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                'Waktu Tayang',
                                style: TextStyle(fontSize: 10.sp),
                              ),
                            ],
                          ),
                          const SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data.releaseDate!.toString(),
                                style: TextStyle(fontSize: 10.sp),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                data.voteCount!.toString(),
                                style: TextStyle(fontSize: 10.sp),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                data.popularity!.toString(),
                                style: TextStyle(fontSize: 10.sp),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                data.runtime!.toString() + ' minute',
                                style: TextStyle(fontSize: 10.sp),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
