import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/home_bloc/widgets/movie_item_card.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: data!.length,
        itemBuilder: (context, index) {
          return MovieItemCard(data: data![index]);
        },
      ),
    );
  }
}
