import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_page.dart';

class MovieItemCard extends StatelessWidget {
  final Results data;
  const MovieItemCard({ Key? key, required this.data }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => DetailMovieScreen(id: data.id!),
        ),
      ),
      child: Card(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(25.0),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(25),
              child: ClipRRect(
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
                child: Image.network(
                    "https://image.tmdb.org/t/p/w500/" + data.posterPath!),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 16,
                left: 25,
                right: 25,
              ),
              child: Text(
                data.title == '' ? data.name! : data.title!,
                style: TextStyle(fontSize: 16.sp),
                textAlign: TextAlign.center,
                textDirection: TextDirection.ltr,
              ),
            )
          ],
        ),
      ),
    );
  }
}