import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/widgets/login_form.dart';
import 'package:majootestcase/ui/login/widgets/register_button.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Fluttertoast.showToast(msg: 'Login Berhasil');
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => const HomeBlocScreen(),
              ),
            );
          } else if (state is AuthBlocErrorState) {
            Fluttertoast.showToast(msg: state.error);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 75,
              left: 25,
              bottom: 25,
              right: 25,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                const Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                const SizedBox(
                  height: 9,
                ),
                const LoginForm(),
                const SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Login',
                  onPressed: () => validate(context),
                  height: 100,
                ),
                const SizedBox(
                  height: 50,
                ),
                const RegisterButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void validate(BuildContext context) async {
    final authBloc = context.read<AuthBlocCubit>();
    if (authBloc.formKey.currentState!.validate()) {
      authBloc.formKey.currentState!.save();
      FocusScope.of(context).unfocus();
      authBloc.loginUser();
    }
  }
}
