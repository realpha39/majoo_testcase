import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: context.read<AuthBlocCubit>().formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: context.read<AuthBlocCubit>().emailController,
            isEmail: true,
            mandatory: false,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              String value = val ?? '';
              if (value.isEmpty) {
                return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
              }
              if (!context.read<AuthBlocCubit>().pattern.hasMatch(value)) {
                return 'Masukkan e-mail yang valid';
              }
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            mandatory: false,
            label: 'Password',
            hint: 'password',
            controller: context.read<AuthBlocCubit>().passwordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              String value = val ?? '';
              if (value.isEmpty) {
                return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
              }
              return null;
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
