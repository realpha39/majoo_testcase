import 'package:flutter/material.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => const RegisterPage(),
          ),
        ),
        child: RichText(
          text: const TextSpan(
            text: 'Belum punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(
                text: 'Daftar',
              ),
            ],
          ),
        ),
      ),
    );
  
  }
}