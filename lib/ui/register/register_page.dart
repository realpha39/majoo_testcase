import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../bloc/register_bloc/register_bloc_cubit.dart';
import '../../common/widget/custom_button.dart';
import '../home_bloc/home_bloc_screen.dart';
import 'widgets/register_form_field_section.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => RegisterBlocCubit(),
        child: const _RegisterBody(),
      ),
    );
  }
}

class _RegisterBody extends StatelessWidget {
  const _RegisterBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBlocCubit, RegisterBlocState>(
      listener: (context, state) {
        if (state is RegisterBlocSuccess) {
          Fluttertoast.showToast(msg: 'Register Berhasil');
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => const HomeBlocScreen(),
            ),
          );
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(
          top: 75,
          left: 25,
          bottom: 25,
          right: 25,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Selamat Datang',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                // color: colorBlue,
              ),
            ),
            const Text(
              'Silahkan register terlebih dahulu',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
              ),
            ),
            const SizedBox(
              height: 9,
            ),
            const RegisterFormFieldSection(),
            const SizedBox(
              height: 50,
            ),
            CustomButton(
              text: 'Register',
              onPressed: () => _validate(context),
              height: 100,
            ),
            const SizedBox(
              height: 50,
            ),
            _register(context)
          ],
        ),
      ),
    );
  }

  Widget _register(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () => Navigator.pop(context),
        child: RichText(
          text: const TextSpan(
            text: 'Sudah punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(
                text: 'Login',
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _validate(BuildContext context) async {
    final authBloc = context.read<RegisterBlocCubit>();
    if (authBloc.formKey.currentState!.validate()) {
      authBloc.formKey.currentState!.save();
      FocusScope.of(context).unfocus();
      authBloc.registerUser();
    }
  }
}
