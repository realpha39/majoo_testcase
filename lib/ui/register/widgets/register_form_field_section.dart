import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';

import '../../../bloc/register_bloc/register_bloc_cubit.dart';

class RegisterFormFieldSection extends StatefulWidget {
  const RegisterFormFieldSection({Key? key}) : super(key: key);

  @override
  State<RegisterFormFieldSection> createState() =>
      _RegisterFormFieldSectionState();
}

class _RegisterFormFieldSectionState extends State<RegisterFormFieldSection> {
  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: context.read<RegisterBlocCubit>().formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: context.read<RegisterBlocCubit>().usernameController,
            mandatory: false,
            hint: 'example',
            label: 'Username',
            validator: (val) {
              String value = val ?? '';
              if (value.isEmpty) {
                return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
              }
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            controller: context.read<RegisterBlocCubit>().emailController,
            isEmail: true,
            mandatory: false,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              String value = val ?? '';
              if (value.isEmpty) {
                return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
              }
              if (!context.read<RegisterBlocCubit>().pattern.hasMatch(value)) {
                return 'Masukkan e-mail yang valid';
              }
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            mandatory: false,
            label: 'Password',
            hint: 'password',
            controller: context.read<RegisterBlocCubit>().passwordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              String value = val ?? '';
              if (value.isEmpty) {
                return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
              }
              return null;
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
