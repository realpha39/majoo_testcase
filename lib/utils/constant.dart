import 'package:flutter/material.dart';

class Preference {
  static const userInfo = "user-info";
}

class Api {
  static const baseUrl = "https://api.themoviedb.org/3/";
  static const login = "/login";
  static const register = "/register";
  static const apiKey = "92753fe384d3ebe6c168e93c0086414e";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class ColorStyle {
  static const Color backgroundColor = Color(0xFFFEF9F9);
  static const Color bgGreyColor = Color(0xFFF0F0F0);
}
